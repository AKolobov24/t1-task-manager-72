<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp" />
<h1>PROJECT LIST</h1>
<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;" >
    <tr>
        <th>ID</th>
        <th>NAME</th>
        <th>DESCRIPTION</th>
        <th>STATUS</th>
        <th>START</th>
        <th>FINISH</th>
        <th>EDIT</th>
        <th>DELETE</th>
    </tr>

    <c:forEach var="project" items = "${projects}" >
        <tr>
            <td>
                <c:out value="${project.id}" />
            </td>
            <td>
                <c:out value="${project.name}" />
            </td>
            <td>
                <c:out value="${project.description}" />
            </td>
            <td align="center">
                <c:out value="${project.status.displayName}" />
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateStart}" />
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}" />
            </td>
            <td align="center">
                <a href="/project/edit?id=${project.id}">EDIT</a>
            </td>
            <td align="center">
                <form name="deleteProjectForm" action="/project/delete" method="POST" style="margin: 0px;">
                    <input type="hidden" name="id" value="${project.id}" />
                    <button type="submit">DELETE</button>
                </form>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp" />