package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.akolobov.tm.web.dto.Result;

@RestController
@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @NotNull
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    Result login(@RequestParam("username") final String login,
                 @RequestParam("password") final String password
    );

    @NotNull
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    Result logout();

}
