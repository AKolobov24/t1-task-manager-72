package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.dto.CustomUser;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @GetMapping("/projects")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser customUser) {
        return new ModelAndView(
                "project-list",
                "projects",
                projectService.findAllByUserId(customUser.getUser().getId())
        );
    }

}
