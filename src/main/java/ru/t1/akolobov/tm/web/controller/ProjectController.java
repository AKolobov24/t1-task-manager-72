package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.dto.CustomUser;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @NotNull
    @GetMapping("/project/create")
    @PreAuthorize("isAuthenticated()")
    public String create(
            @AuthenticationPrincipal final CustomUser customUser
    ) {
        @NotNull final Project project = new Project();
        project.setUser(customUser.getUser());
        projectService.save(project);
        return "redirect:/project/edit?id=" + project.getId();
    }

    @NotNull
    @PostMapping("/project/delete")
    @PreAuthorize("isAuthenticated()")
    public String delete(
            @AuthenticationPrincipal final CustomUser customUser,
            @RequestParam(name = "id") @NotNull final String id
    ) {
        projectService.deleteByUserIdAndId(customUser.getUser().getId(), id);
        return "redirect:/projects";
    }

    @NotNull
    @GetMapping("/project/edit")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @RequestParam(name = "id") @NotNull final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        @Nullable final Project project = projectService.findByUserIdAndId(customUser.getUser().getId(), id);
        if (project == null) {
            modelAndView.setStatus(HttpStatus.valueOf(404));
            return modelAndView;
        }
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/project/edit")
    @PreAuthorize("isAuthenticated()")
    public String edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @ModelAttribute("project") @NotNull final Project project
    ) {
        project.setUser(customUser.getUser());
        projectService.save(project);
        return "redirect:/projects";
    }

}
