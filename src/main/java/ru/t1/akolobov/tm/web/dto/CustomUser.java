package ru.t1.akolobov.tm.web.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.t1.akolobov.tm.web.model.User;

import java.util.Collection;

public class CustomUser extends org.springframework.security.core.userdetails.User {

    private User user;

    public CustomUser(@NotNull final UserDetails userDetails) {
        super(userDetails.getUsername(),
                userDetails.getPassword(),
                userDetails.isEnabled(),
                userDetails.isAccountNonExpired(),
                userDetails.isCredentialsNonExpired(),
                userDetails.isAccountNonLocked(),
                userDetails.getAuthorities()
        );
    }

    @NotNull
    public CustomUser withUser(@NotNull final User user) {
        this.user = user;
        return this;
    }

    public CustomUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public CustomUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public User getUser() {
        return user;
    }

    public void setUser(@NotNull final User user) {
        this.user = user;
    }

}
