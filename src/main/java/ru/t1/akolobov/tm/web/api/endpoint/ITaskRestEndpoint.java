package ru.t1.akolobov.tm.web.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.t1.akolobov.tm.web.model.Task;

import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @NotNull
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    Collection<Task> findAll();

    @Nullable
    @GetMapping("/findById/{id}")
    @PreAuthorize("isAuthenticated()")
    Task findById(
            @PathVariable("id")
            @NotNull final String id
    );

    @Nullable
    @PostMapping("/save")
    @PreAuthorize("isAuthenticated()")
    Task save(
            @RequestBody
            @NotNull final Task task
    );

    @PostMapping("/deleteById/{id}")
    @PreAuthorize("isAuthenticated()")
    void deleteById(
            @PathVariable("id")
            @NotNull final String id
    );

}
