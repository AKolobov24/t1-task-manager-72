package ru.t1.akolobov.tm.web.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.akolobov.tm.web.model.AbstractModel;

import java.util.Collection;

@NoRepositoryBean
public interface CommonRepository<M extends AbstractModel> extends JpaRepository<M, String> {

    Collection<M> findAllByUserId(@NotNull final String userId);

    M findByUserIdAndId(@NotNull final String userId, @NotNull final String Id);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String Id);

    void deleteAllByUserId(@NotNull final String userId);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String Id);

}
