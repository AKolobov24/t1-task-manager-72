package ru.t1.akolobov.tm.web.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.akolobov.tm.web.model.User;
import ru.t1.akolobov.tm.web.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @NotNull
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    public User findById(@NotNull final String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Transactional
    public User save(@NotNull final User user) {
        return userRepository.save(user);
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        if (!userRepository.existsById(id)) throw new EntityNotFoundException();
        userRepository.deleteById(id);
    }

    public boolean existsByLogin(@NotNull final String login) {
        return userRepository.existsByLogin(login);
    }

}
