package ru.t1.akolobov.tm.web.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.akolobov.tm.web.api.service.IProjectService;
import ru.t1.akolobov.tm.web.api.service.ITaskService;
import ru.t1.akolobov.tm.web.dto.CustomUser;
import ru.t1.akolobov.tm.web.enumerated.Status;
import ru.t1.akolobov.tm.web.model.Task;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @NotNull
    @GetMapping("/task/create")
    @PreAuthorize("isAuthenticated()")
    public String create(@AuthenticationPrincipal final CustomUser customUser) {
        @NotNull final Task task = new Task();
        task.setUser(customUser.getUser());
        taskService.save(task);
        return "redirect:/task/edit?id=" + task.getId();
    }

    @NotNull
    @PostMapping("/task/delete")
    @PreAuthorize("isAuthenticated()")
    public String delete(
            @AuthenticationPrincipal final CustomUser customUser,
            @RequestParam(name = "id") @NotNull final String id
    ) {
        taskService.deleteByUserIdAndId(customUser.getUser().getId(), id);
        return "redirect:/tasks";
    }

    @NotNull
    @GetMapping("/task/edit")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @RequestParam(name = "id") @NotNull final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        @Nullable final Task task = taskService.findByUserIdAndId(customUser.getUser().getId(), id);
        if (task == null) {
            modelAndView.setStatus(HttpStatus.valueOf(404));
            return modelAndView;
        }
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("statuses", Status.values());
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/edit")
    @PreAuthorize("isAuthenticated()")
    public String edit(
            @AuthenticationPrincipal final CustomUser customUser,
            @ModelAttribute("task") @NotNull final Task task
    ) {
        if (task.getProjectId() != null && task.getProjectId().isEmpty())
            task.setProjectId(null);
        task.setUser(customUser.getUser());
        taskService.save(task);
        return "redirect:/tasks";
    }

}
