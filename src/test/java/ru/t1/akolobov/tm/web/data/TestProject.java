package ru.t1.akolobov.tm.web.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.web.model.Project;
import ru.t1.akolobov.tm.web.model.User;

import java.util.ArrayList;
import java.util.List;

public final class TestProject {

    @NotNull
    public static Project createProject() {
        return new Project("test-project");
    }

    @NotNull
    public static Project createProject(@NotNull final User user) {
        @NotNull Project project = createProject();
        project.setUser(user);
        return project;
    }

    @NotNull
    public static List<Project> createProjectList(@NotNull final User user) {
        @NotNull List<Project> projectList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull Project project = new Project("test-project-" + i);
            project.setUser(user);
            projectList.add(project);
        }
        return projectList;
    }

}
